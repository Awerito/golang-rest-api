# Golang RESTfull-API

API for the management of albums data. Data is store on memory.

Exercise of the [Tutorial: Developing a RESTful API with Go and
Gin](https://golang.org/doc/tutorial/web-service-gin).

## Execution

    $ go run .
